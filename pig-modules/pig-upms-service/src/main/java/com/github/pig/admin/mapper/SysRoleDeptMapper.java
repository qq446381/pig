package com.github.pig.admin.mapper;

import com.github.pig.admin.model.entity.SysRoleDept;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 角色与部门对应关系 Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2018-01-20
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}